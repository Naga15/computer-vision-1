import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import convolve2d


def load_data(path):
    '''
    Load data from folder data, face images are in the folder facial_images, face features are in the folder facial_features.
    

    Args:
        path: path of folder data

    Returns:
        imgs: list of face images as numpy arrays 
        feats: list of facial features as numpy arrays 
    '''

    imgs = []
    feats = []

    # hmmm... there is in main.py is root=data so we need to access folders and load the nps to list
    # using plt.imread but in documentation says that we should use Pillow
    # "This function exists for historical reasons. It is recommended to use PIL.Image.open instead for loading images.
    # "

    img_folder = '{}/facial_images'.format(path)
    feat_folder = '{}/facial_features'.format(path)

    for root, dirs, files in os.walk(img_folder, topdown=True):
        for name in files:
            imgs.append(plt.imread('{}/{}'.format(img_folder, name)))

    for root, dirs, files in os.walk(feat_folder, topdown=True):
        for name in files:
            feats.append(plt.imread('{}/{}'.format(feat_folder, name)))

    #

    return imgs, feats


def gaussian_kernel(fsize, sigma):
    '''
    Define a Gaussian kernel

    Args:
        fsize: kernel size
        sigma: sigma of Gaussian kernel

    Returns:
        The Gaussian kernel
    '''

    # filter is same as kernel in 2D, I will use code from Assignment 1 problem 4 ? should be normalized?(ask)
    # width and height
    width = fsize
    height = fsize
    # init
    gauss_array = np.zeros((height, width))
    # maxmial value
    gauss_max = 1 / (2 * np.pi * sigma ** 2)
    # x,y values
    if width == 1:
        x = np.array([0])
    else:
        x = np.linspace(-1.0, 1.0, num=width)

    if height == 1:
        y = np.array([0])
    else:
        y = np.linspace(-1.0, 1.0, num=height)

    for i in range(height):
        for j in range(width):
            gauss_array[i][j] = gauss_max * np.exp(-(x[j] ** 2 + y[(height - 1) - i] ** 2) / (2 * sigma ** 2))

    #

    return gauss_array / np.sum(gauss_array)


def downsample_x2(x, factor=2):
    '''
    Downsampling an image by a factor of 2

    Args:
        x: image as numpy array (H * W)

    Returns:
        downsampled image as numpy array (H/2 * W/2)
    '''

    #
    # TODO
    # it looks to easy

    downsample = x[::factor, ::factor]

    return downsample


def gaussian_pyramid(img, nlevels, fsize, sigma):
    '''
    A Gaussian pyramid is constructed by combining a Gaussian kernel and downsampling.
    Tips: use scipy.signal.convolve2d for filtering image.

    Args:
        img: face image as numpy array (H * W)
        nlevels: number of levels of Gaussian pyramid, in this assignment we will use 3 levels
        fsize: Gaussian kernel size, in this assignment we will define 5
        sigma: sigma of Gaussian kernel, in this assignment we will define 1.4

    Returns:
        GP: list of Gaussian downsampled images, it should be 3 * H * W
    '''
    GP = []
    img_tmp = img
    gauss = gaussian_kernel(fsize, sigma)
    GP.append(img)
    #
    for i in range(1, nlevels):
        img_tmp = convolve2d(img_tmp, gauss, boundary='symm', mode='same')
        img_tmp = downsample_x2(img_tmp)
        GP.append(img_tmp)
    #

    return GP


def template_distance(v1, v2):
    '''
    Calculates the distance between the two vectors to find a match.
    Browse the course slides for distance measurement methods to implement this function.
    Tips: 
        - Before doing this, let's take a look at the multiple choice questions that follow. 
        - You may need to implement these distance measurement methods to compare which is better.

    Args:
        v1: vector 1
        v2: vector 2

    Returns:
        Distance
    '''
    distance = None

    # dot product
    dot_prod = np.dot(v1, v2) # v1^T @ v2
    v1_norm = np.linalg.norm(v1) # ||v1||
    v2_norm = np.linalg.norm(v2)  # ||v2||
    distance = dot_prod / (v1_norm * v2_norm) #(v1^T @ v2)/||v1||*||v2||

    # SSD
    # distance = np.dot(v1 - v2, v1 - v2)
    # sum((v1-v2)^2) -> (v1-v2)^T @ (v1-v2)
    return distance


def sliding_window(img, feat, step=1):
    ''' 
    A sliding window for matching features to windows with SSDs. When a match is found it returns to its location.
    
    Args:
        img: face image as numpy array (H * W)
        feat: facial feature as numpy array (H * W)
        step: stride size to move the window, default is 1
    Returns:
        min_score: distance between feat and window
    '''

    min_score = None

    #
    h_img, w_img = np.shape(img)
    h_feat, w_feat = np.shape(feat)
    #
    # sliding window over the image with some loops
    # extracting the submatrix that should match to a feature
    # it will slide and analyzed with step
    i = 0
    j = 0
    # I will set minimal score on one million- we want minimal so 0
    min_score = 1000000
    while h_img > i + h_feat - 1:  # if the window is on a boundary stop
        while w_img > j + w_feat - 1:
            window = img[i:i + h_feat , j:j + w_feat ]  # creating a window
            assert (np.shape(window) == np.shape(feat))  # checking the size
            score = template_distance(window.flatten().T, feat.flatten().T)  # I want column vector
            if score < min_score:
                min_score = score  # saving the minimal score
            j = j + step  # next step
        j = 0  # while breaks if it goes over the boundary
        i = i + step  # next height idx
    return min_score  # need to test


class Distance(object):
    # choice of the method
    METHODS = {1: 'Dot Product', 2: 'SSD Matching'}

    # choice of reasoning
    REASONING = {
        1: 'it is more computationally efficient',
        2: 'it is less sensitive to changes in brightness.',
        3: 'it is more robust to additive Gaussian noise',
        4: 'it can be implemented with convolution',
        5: 'All of the above are correct.'
    }

    def answer(self):
        '''Provide your answer in the return value.
        This function returns one tuple:
            - the first integer is the choice of the method you will use in your implementation of distance.
            - the following integers provide the reasoning for your choice.
        Note that you have to implement your choice in function template_distance

        For example (made up):
            (1, 1) means
            'I will use Dot Product because it is more computationally efficient.'
        '''

        return (2, 1)  # ssd it is just one row of code and it should be efficient
        # from mathematical prespective it is just squared error so there  (4) isn't


def find_matching_with_scale(imgs, feats):
    ''' 
    Find face images and facial features that match the scales 
    
    Args:
        imgs: list of face images as numpy arrays
        feats: list of facial features as numpy arrays 
    Returns:
        match: all the found face images and facial features that match the scales: N * (score, g_im, feat)
        score: minimum score between face image and facial feature
        g_im: face image with corresponding scale
        feat: facial feature
    '''
    match = []
    #(score, g_im, feat) = (None, None, None)

    # we need loops- images an features match will have num_images*num_feats size

    for img in imgs:
        img_pyramid = gaussian_pyramid(img, 3, 5, 1.4)  # nlvls -3 size -5 ans sigma 1.4 like in other functions
        #now current feature
        for pyr in img_pyramid:
            ## match feats at each img n pyramid
            for feat in feats:
                match.append((sliding_window(pyr,feat) ,pyr, feat))


    #

    return match
