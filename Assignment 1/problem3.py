import numpy as np
import scipy.linalg


def load_points(path):
    '''
    Load points from path pointing to a numpy binary file (.npy). 
    Image points are saved in 'image'
    Object points are saved in 'world'

    Returns:
        image: A Nx2 array of 2D points form image coordinate 
        world: A N*3 array of 3D points form world coordinate
    '''
    #

    points = np.load(path)
    image = points['image']
    world = points['world']
    # they are homogenous
    #  possible mistake in declaration up there
    # line 13 'world: A N*3 array of 3D points form world coordinate'
    # suggestion:
    # line 13 'world: A Nx3 array of 3D points form world coordinate'
    # with 1D array N*3 there is error in function create_A

    # dimmension problems in create_A is stated nothing about homogenism so i will only load
    # please correct this return decleration as
    # Returns:
    #         image: A Nx3 array of 2D points form image coordinate
    #         world: A Nx4 array of 3D points form world coordinate
    # it is really annoying
    assert (np.size(image, 0) == np.size(world, 0))
    assert (np.size(image, 1) == 3 and np.size(world, 1) == 4)
    return image, world


#

def create_A(x, X):
    """Creates (2*N, 12) matrix A from 2D/3D correspondences
    that comes from cross-product
    
    Args:
        x and X: N 2D and 3D point correspondences (homogeneous)
        
    Returns:
        A: (2*N, 12) matrix A
    """

    N, _ = x.shape
    assert N == X.shape[0]
    #  Matrix building 2*N why not 3*N like in the lecture? I will ignore the last row of the matrix(hopefully will be good)
    A = np.zeros((2 * N, 12))  # return as stated above

    #  building the matrix [0 , -Xi', y*Xi'; Xi, 0 , -x*Xi]- no third row because - 2*N
    for point in range(N):
        xx = x[point][0]
        yy = x[point][1]
        Xi = X[point][:]
        A[2*point][0:4] = [0, 0, 0, 0]  # null
        A[2*point][4:8] = -Xi
        A[2*point][8:12] = yy * Xi
        A[2*point + 1][0:4] = Xi
        A[2*point + 1][4:8] = [0, 0, 0, 0]  # null
        A[2*point + 1][8:12] = -xx * Xi

    return A

    #


def homogeneous_Ax(A):
    """Solve homogeneous least squares problem (Ax = 0, s.t. norm(x) == 0),
    using SVD decomposition as in the lecture.

    Args:
        A: (2*N, 12) matrix A
    
    Returns:
        P: (3, 4) projection matrix P
    """
    # SVD
    # sigmas are ordered by size and a smallest is on the end

    P = np.empty((3, 4))
    u, s, vh = np.linalg.svd(A)
    smallest = np.argmin(s)
    P = vh[smallest, :].reshape((3, 4))  # last column and reshape to matrix as stated in lecture
    return P
    #


def solve_KR(P):
    """Using th RQ-decomposition find K and R 
    from the projection matrix P.
    Hint 1: you might find scipy.linalg useful here.
    Hint 2: recall that K has 1 in the the bottom right corner.
    Hint 3: RQ decomposition is not unique (up to a column sign).
    Ensure positive element in K by inverting the sign in K columns 
    and doing so correspondingly in R.

    Args:
        P: 3x4 projection matrix.
    
    Returns:
        K: 3x3 matrix with intrinsics
        R: 3x3 rotation matrix 
    """
    K = np.empty((3, 3))
    R = np.empty((3, 3))
    # RQ decomposition need quadratic matrix
    P_rq = P[:3, :3]
    K, R = scipy.linalg.rq(P_rq, mode='full')
    # doing with math
    #  K * R = A -> K_t * R_t =A
    # formula: K @ T @ T_inv @ R = A
    # K_t = abs(K)-> positive matrix
    K_t = np.abs(K)
    T_inv = scipy.linalg.solve(K_t, K)
    #  K_t = K * T ; R_t = T^-1 * R
    R_t = T_inv @ R
    #  K_norm = K_t/K[2][2]; R_norm = R_t *K[2][2]  K[2][2]/K[2][2]= 1, A is still same like before
    K_norm = K_t/K_t[2][2]
    R_norm = R_t * K_t[2][2]

    return K_norm, R_norm

def solve_c(P):
    """Find the camera center coordinate from P
    by finding the nullspace of P with SVD.

    Args:
        P: 3x4 projection matrix
    
    Returns:
        c: 3x1 camera center coordinate in the world frame
    """
    #
    u, s, vh = np.linalg.svd(P)
    smallest = np.argmin(s)
    c_t = vh[smallest, :]
    # c has length 4
    tmp = c_t
    tmp=tmp/c_t[-1] # homognize order 4 to get non-homogeous 3
    c = tmp[:-1].reshape(3, 1)


    return c
