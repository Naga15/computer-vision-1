import math
import numpy as np
from scipy import ndimage


def gauss2d(sigma, fsize):
    """
  Args:
    sigma: width of the Gaussian filter
    fsize: dimensions of the filter

  Returns:
    g: *normalized* Gaussian filter
  """

    #
    # width and height
    width = fsize[0]
    height = fsize[1]
    # init
    gauss_array = np.zeros((height, width))
    # maximal value
    gauss_max = 1 / (2 * np.pi * sigma ** 2)
    # x,y values
    # trivial
    if width == 1:
        x = np.array([0])
    # 1D array of x values
    else:
        x = np.linspace(-1.0, 1.0, num=width)
    # trivial
    if height == 1:
        y = np.array([0])
    # 1D array of y values
    else:
        y = np.linspace(-1.0, 1.0, num=height)
    # compute filter
    for i in range(height):
        for j in range(width):
            gauss_array[i][j] = gauss_max * np.exp(-(x[j] ** 2 + y[(height - 1) - i] ** 2) / (2 * sigma ** 2))

    return gauss_array / np.sum(gauss_array)  # normalize
    #


def createfilters():
    """
  Returns:
    fx, fy: filters as described in the problem assignment
  """

    # 1 x 3 array for central diff in x
    dx = 0.5 * np.array([-1, 0, 1])
    # gauss in y direction
    gaussx = gauss2d(0.9, (3, 1))
    fx = np.outer(gaussx, dx)
    dy = dx.reshape(1, 3)
    gaussy = gauss2d(0.9, (1, 3))
    fy = np.outer(dy, gaussy)
    return fx, fy


def filterimage(I, fx, fy):
    """ Filter the image with the filters fx, fy.
  You may use the ndimage.convolve scipy-function.

  Args:
    I: a (H,W) numpy array storing image data
    fx, fy: filters

  Returns:
    Ix, Iy: images filtered by fx and fy respectively
  """

    #
    Ix = ndimage.convolve(I, fx)
    Iy = ndimage.convolve(I, fy)
    return Ix, Iy
    #


def detectedges(Ix, Iy, thr):
    """ Detects edges by applying a threshold on the image gradient magnitude.

  Args:
    Ix, Iy: filtered images
    thr: the threshold value

  Returns:
    edges: (H,W) array that contains the magnitude of the image gradient at edges and 0 otherwise
  """

    #

    grad_magnitude = np.sqrt(Ix ** 2 + Iy ** 2)
    edges = grad_magnitude
    edges[grad_magnitude < thr] = 0

    return edges
    #


def nonmaxsupp(edges, Ix, Iy):
    """ Performs non-maximum suppression on an edge map.

  Args:
    edges: edge map containing the magnitude of the image gradient at edges and 0 otherwise
    Ix, Iy: filtered images

  Returns:
    edges2: edge map where non-maximum edges are suppressed
  """
    # Your code here
    # This part, I followed the tutorial from Sofiane Sahir.
    # https://towardsdatascience.com/canny-edge-detection-step-by-step-in-python-computer-vision-b49c3a2d8123.

    # Calculate the angle in degree.
    theta = np.arctan2(Iy, Ix)
    angle = theta * 180 / np.pi

    # Create an empty matrix, same size to original gradient matrix.
    # Its values are float between 0-1.
    H, W = edges.shape
    edges2 = np.zeros((H, W))

    for i in range(1, H - 1):
        for j in range(1, W - 1):
            try:
                # If the pixel is right, set its pixel intensity (between 0–1) to 1.0.
                q = 0
                r = 0

                # check if the pixels on the same direction are more or less intense than the ones being processed.

                # left-to-right edges: theta in (-22.5, 22.5]
                if -22.5 < angle[i, j] <= 22.5:
                    q = edges[i, j + 1]
                    r = edges[i, j - 1]

                # bottomleft-to-topright edges: theta in (22.5, 67.5]
                elif 22.5 < angle[i, j] <= 67.5:
                    q = edges[i + 1, j - 1]
                    r = edges[i - 1, j + 1]

                # top-to-bottom edges: theta in [-90, -67.5] or (67.5, 90]
                elif (-90 <= angle[i, j] < -67.5) or (67.5 < angle[i, j] <= 90):
                    q = edges[i + 1, j]
                    r = edges[i - 1, j]

                # topleft-to-bottomright edges: theta in [-67.5, -22.5]
                elif -67.5 <= angle[i, j] < -22.5:
                    q = edges[i - 1, j - 1]
                    r = edges[i + 1, j + 1]

                if (edges[i, j] >= q) and (edges[i, j] >= r):
                    edges2[i, j] = edges[i, j]
                else:
                    edges2[i, j] = 0

            except IndexError as e:
                pass
    return edges2
