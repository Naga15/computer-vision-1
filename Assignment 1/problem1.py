import numpy as np
import matplotlib.pyplot as plot


def display_image(img):
    """ Show an image with matplotlib:

    Args:
        Image as numpy array (H,W,3)
    """

    #
    plot.imshow(img)
    plot.show()
    #


def save_as_npy(path, img):
    """ Save the image array as a .npy file:

    Args:
        Image as numpy array (H,W,3)
    """

    #
    np.save(path,img)
    #


def load_npy(path):
    """ Load and return the .npy file:

    Args:
        Path of the .npy file
    Returns:
        Image as numpy array (H,W,3)
    """

    #
    return np.load(path)
    #


def mirror_horizontal(img):
    """ Create and return a horizontally mirrored image:

    Args:
        Loaded image as numpy array (H,W,3)

    Returns:
        A horizontally mirrored numpy array (H,W,3).
    """

    #
    return np.fliplr(img)
    #


def display_images(img1, img2):
    """ display the normal and the mirrored image in one plot:

    Args:
        Two image numpy arrays
    """

    #
    fig = plot.figure()
    fig.add_subplot(1,2,1)
    plot.imshow(img1)
    fig.add_subplot(1,2,2)
    plot.imshow(img2)
    fig.suptitle('normal and mirrored')
    plot.show()
    #
