import numpy as np
from scipy.ndimage import convolve


def loaddata(path):
    """ Load bayerdata from file

    Args:
        Path of the .npy file
    Returns:
        Bayer data as numpy array (H,W)
    """

    #
    return np.load(path)
    #


def separatechannels(bayerdata):
    """ Separate bayer data into RGB channels so that
    each color channel retains only the respective
    values given by the bayer pattern and missing values
    are filled with zero

    Args:
        Numpy array containing bayer data (H,W)
    Returns:
        red, green, and blue channel as numpy array (H,W)
    """

    # disassembling  the image on rgb bayergrid case.
    rot = np.copy(bayerdata)
    grun = np.copy(bayerdata)
    blau = np.copy(bayerdata)

    rows = np.size(bayerdata, 0)
    columns = np.size(bayerdata, 1)

    for row in range(rows):
        for column in range(columns):
            # positioning of the colors
            blue = row % 2 == 1 and column % 2 == 0  # blue pixel reserved
            red = row % 2 == 0 and column % 2 == 1  # red pixel reserved
            green = (column + row % 2) % 2 == 0   # green pixel reserved
            # if found all other don't exist
            if blue:
                grun[row, column] = 0
                rot[row, column] = 0
            elif red:
                grun[row, column] = 0
                blau[row, column] = 0
            elif green:
                blau[row, column] = 0
                rot[row, column] = 0

    return rot, grun, blau
    #


def assembleimage(r, g, b):
    """ Assemble separate channels into image

    Args:
        red, green, blue color channels as numpy array (H,W)
    Returns:
        Image as numpy array (H,W,3)
    """

    # those channels need to be merged in one image
    H = np.size(r, 0)
    W = np.size(r, 1)
    image_data = np.zeros((H, W, 3)) # H x W pixel grid with rgb(3) information for each pixel
    for row in range(H):
        for column in range(W):
            image_data[row, column] = [r[row, column], g[row, column], b[row, column]]  # insert the rgb values
    return image_data
    #


def interpolate(r, g, b):
    """ Interpolate missing values in the bayer pattern
    by using bilinear interpolation

    Args:
        red, green, blue color channels as numpy array (H,W)
    Returns:
        Interpolated image as numpy array (H,W,3)
    """

    #  we need kernels
    red_kernel = np.array([[1/4, 1/2, 1/4], [1/2, 1, 1/2], [1/4, 1/2, 1/4]])
    green_kernel = np.array([[1/5, 1/4, 1/5], [1/4, 1/5, 1/4], [1/5, 1/4, 1/5]])  # easy to read
    blue_kernel = red_kernel  # same

    inter_red = convolve(r, red_kernel, mode="mirror")
    inter_green = convolve(g, green_kernel, mode="mirror")
    inter_blue = convolve(b, blue_kernel,mode="mirror")

    return assembleimage(inter_red, inter_green, inter_blue)
    #
